# frozen_string_literal: true

require 'resque/server'

Rails.application.routes.draw do
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/signup', to: 'users#new'
  get 'welcome/index'
  root to: 'welcome#index'
  resources :users do
    member do
      get :following, :followers, :received_comments
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets, only: %i[new create edit update]
  resources :posts, only: %i[create destroy update edit show]
  resources :relationships, only: %i[create destroy]
  resources :reviews
  resources :conversations, only: %i[create destroy edit]
  resources :observerships, only: %i[create destroy]
  get '/display', to: 'noticeboards#show'
  get '/noticeboards/refresh_part'

  mount Resque::Server.new, at: '/jobs'
end
