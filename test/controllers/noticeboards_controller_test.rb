require 'test_helper'

class NoticeboardsControllerTest < ActionDispatch::IntegrationTest
  test 'should redirect show when not logged in' do
    get '/display'
    assert_redirected_to login_url
  end
end
