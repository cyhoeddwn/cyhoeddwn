require 'test_helper'

class ConversationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
    @other_user = users(:samwise)
  end

  test 'create should require logged-in user' do
    assert_no_difference 'Conversation.count' do
      post conversations_path
    end
    assert_redirected_to login_url
  end

  test 'destroy should require logged-in user' do
    assert_no_difference 'Conversation.count' do
      delete conversation_path(conversations(:one))
    end
    assert_redirected_to login_url
  end

  test 'disallow destroy non-user comment' do
    log_in_as(@user)
    comment = conversations(:two)
    assert_no_difference 'Conversation.count' do
      delete conversation_path(comment)
    end
    assert_redirected_to root_url
  end
end
