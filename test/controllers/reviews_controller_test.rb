# frozen_string_literal: true

require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
    @post = posts(:apple)
    @review = @post.reviews.build(comment: 'test', approved: true)
    @user.add_review(@review)
    @review.save
  end

  test 'should not allow new review for same user' do
    log_in_as(@user)
    get new_review_path(id: @post.id)
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test 'should not allow create review for same user' do
    log_in_as(@user)
    assert_no_difference 'Review.count' do
      post reviews_path params: { review: { comment: 'some test', approved: true }, post_id: @post.id }
    end
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test 'should redirect new review when not logged in' do
    get new_review_path(id: @post.id)
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test 'should redirect create review when not logged in' do
    assert_no_difference 'Review.count' do
      post reviews_path params: { review: { comment: 'some test', approved: true }, post_id: @post.id }
    end
    assert_not flash.empty?
    assert_redirected_to login_path
  end
end
