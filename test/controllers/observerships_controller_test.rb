require "test_helper"

class ObservershipsControllerTest < ActionDispatch::IntegrationTest
  test 'create should require logged-in user' do
    assert_no_difference 'Observership.count' do
      post observerships_path
    end
    assert_redirected_to login_url
  end

  test 'destroy should require logged-in user' do
    assert_no_difference 'Observership.count' do
      delete observership_path(observerships(:one))
    end
    assert_redirected_to login_url
  end
end
