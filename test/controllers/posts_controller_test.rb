# frozen_string_literal: true

require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @post = posts(:apple)
  end

  test 'should redirect create when not logged in' do
    assert_no_difference 'Post.count' do
      post posts_path, params: { post: { content: 'lipsum' } }
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy when not logged in' do
    assert_no_difference 'Post.count' do
      delete post_path(@post)
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy for wrong post' do
    log_in_as(users(:bilbo))
    post = posts(:hewwo)
    assert_no_difference 'Post.count' do
      delete post_path(post)
    end
    assert_redirected_to root_url
  end

  test 'should disallow update for wrong post' do
    user = users(:bilbo)
    log_in_as(users(:gandalf))
    post = user.posts.paginate(page: 1).first
    content = 'some new content'
    patch post_path(post), params: { post: { content: content } }
    follow_redirect!
    get user_path(user)
    assert_no_match content, response.body
  end
end
