require 'test_helper'

class NoticeboardsDisplayTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
    @no_following = users(:gollum)
  end

  test 'no following should yield no posts partial' do
    log_in_as(@no_following)
    get '/display'
    assert_template partial: '_no_posts'
  end
end
