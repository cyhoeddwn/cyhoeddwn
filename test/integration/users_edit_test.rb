# frozen_string_literal: true

require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
  end

  test 'unsuccesful edit' do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: '',
                                              email: 'foo@invalid',
                                              password: 'foo',
                                              password_confirmation: 'bar' } }
    assert_template 'users/edit'
  end

  test 'successful edit with friendly forwarding' do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name = 'Foo Bar'
    email = 'foo@bar.com'
    bio = 'This is an example bio'
    patch user_path(@user), params: { user: { name: name,
                                              email: email,
                                              password: '',
                                              password_confirmation: '',
                                              bio: bio } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
    assert_equal bio, @user.bio
  end

  test 'clearing bio should resort in template being shown' do
    get edit_user_path(@user)
    log_in_as(@user)
    patch user_path(@user), params: { user: { bio: nil } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    follow_redirect!
    assert_match 'is a cyhoeddwn user', response.body
  end
end
