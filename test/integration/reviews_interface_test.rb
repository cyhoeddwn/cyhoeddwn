# frozen_string_literal: true

require 'test_helper'

class ReviewsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @review_author = users(:saruman)
    @post = posts(:big_bong)
    @post_author = @post.user
    @another_user = users(:merry)
  end

  test 'review interface' do
    log_in_as(@review_author)
    get post_path(@post)
    assert_match @post.content, response.body
    assert_select 'a', text: 'Create review'
    get new_review_path(id: @post.id)
    # Invalid (blank) review
    assert_no_difference 'Review.count' do
      post reviews_path params: { review: { comment: '',
                                            approved: true },
                                  post_id: @post.id }
    end
    assert_select 'div#error-explanation'
    # Correct review
    assert_difference 'Review.count', 1 do
      post reviews_path params: { review: { comment: 'some test',
                                            approved: true },
                                  post_id: @post.id }
    end
    # Delete review
    get post_path(@post)
    assert_select 'a', text: 'Delete review.'
    @review = @post.reviews.first
    assert_difference 'Review.count', -1 do
      delete review_path(@review)
    end
    # No delete review link for user that hasn't left one
    log_in_as(@another_user)
    get post_path(@post)
    assert_select 'a', text: 'Delete review', count: 0
  end
end
