require 'test_helper'

class ReviewWorkflowTest < ActionDispatch::IntegrationTest
  def setup
    @post = posts(:for_review)
    @users = users(:saruman, :merry, :pippin)
    @many_users = users
    ActionMailer::Base.deliveries.clear
  end

  test '3 minimum reviews then revoke review' do
    assert_not @post.approval_status, true
    @users.each do |user|
      log_in_as(user)
      post reviews_path params: { review: { comment: "#{user.name} says ok!",
                                            approved: true },
                                  post_id: @post.id }
    end
    @post.reload
    assert @post.approval_status, true
    assert_equal 1, ActionMailer::Base.deliveries.size
    @review = @post.reviews.first
    delete review_path(@review)
    @post.reload
    assert_not @post.approval_status, true
  end

  test 'give even number of types of review then revoke a negative one' do
    @many_users.select! { |user| user.email.starts_with?('user') }
    @many_users[0..14].each do |user|
      log_in_as(user)
      post reviews_path params: { review: { comment: "#{user.name} says ok!",
                                            approved: true },
                                  post_id: @post.id }
    end
    @many_users[15..].each do |user|
      log_in_as(user)
      post reviews_path params: { review: { comment: "#{user.name} says no!",
                                            approved: false },
                                  post_id: @post.id }
    end
    @post.reload
    assert_not @post.approval_status, true
    @review = @post.reviews.first
    delete review_path(@review)
    @post.reload
    assert @post.approval_status, true
  end

  test 'updating review revokes approval status' do
    assert_not @post.approval_status, true
    @users.each do |user|
      log_in_as(user)
      post reviews_path params: { review: { comment: "#{user.name} says ok!",
                                            approved: true },
                                  post_id: @post.id }
    end
    @post.reload
    assert @post.approval_status, true
    log_in_as(@post.user)
    assert_difference '@post.reviews.count', -3 do
      patch post_path(@post), params: { post: { content: 'an update' } }
    end
    @post.reload
    assert_not @post.approval_status, true
  end
end
