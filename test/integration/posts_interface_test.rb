# frozen_string_literal: true

require 'test_helper'

# TODO(Genevieve): Add a test for image upload (13.65)

class PostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
    @post = posts(:big_bong)
    ActionMailer::Base.deliveries.clear
  end

  test 'post interface' do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    # Invalid submission
    assert_no_difference 'Post.count' do
      post posts_path, params: { post: { content: '' } }
    end
    assert_select 'div#error-explanation'
    # TODO(Genevieve): Fix this assert select statement  `assert_select 'a[href=?]', '/?page=2'`
    # Valid submission
    content = 'This post really ties the room together'
    image = fixture_file_upload('test/fixtures/linfox2bg.png', 'image/png')
    assert_difference 'Post.count', 2 do
      post posts_path, params: { post: { content: content } }
      post posts_path, params: { post: { content: content, image: image } }
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    assert_equal 2, ActionMailer::Base.deliveries.size
    # Invalid image submissions
    images = [{ image: 'mr_flibble.gif', mime: 'image/gif' },
              { image: 'froggers.jpg', mime: 'image/jpeg' }]
    images.each do |file|
      image = fixture_file_upload("test/fixtures/#{file[:image]}", file[:mime])
      assert_no_difference 'Post.count' do
        post posts_path, params: { post: { content: content, image: image } }
      end
    end
    # Delete post
    assert_select 'a', text: 'Delete post.'
    first_post = @user.posts.paginate(page: 1).first
    other_post = @user.posts.paginate(page: 1).last
    assert_difference 'Post.count', -1 do
      delete post_path(first_post)
    end
    # Visit different user (no delete links)
    get user_path(users(:gandalf))
    assert_select 'a', text: 'delete', count: 0
    # Edit post
    get user_path(@user)
    assert_select 'a', text: 'Edit post.'
    content = 'some new content here'
    patch post_path(other_post), params: { post: { content: content } }
    follow_redirect!
    # assert_match content, response.body
  end

  test 'post sidebar count' do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.posts.count} posts", response.body
    # User with zero posts
    other_user = users(:saruman)
    log_in_as(other_user)
    get root_path
    assert_match '0 posts', response.body
    other_user.posts.create!(content: 'an example post')
    get root_path
    assert_match '1 post', response.body
  end
end
