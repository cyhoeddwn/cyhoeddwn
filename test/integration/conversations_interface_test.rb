require "test_helper"

class ConversationsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
    @other_user = users(:samwise)
  end

  test 'conversations interface' do
    log_in_as(@user)
    # Check user profile has a talk page link
    get user_url(@user)
    assert_select 'a', text: "View talk page (#{@user.received_comments.count})"
    get received_comments_user_path(@user)
    # Check there are links to delete own comment
    assert_select 'a', text: 'Delete comment.', count: 1
    # Check other user can delete their comment
    log_in_as(@other_user)
    get received_comments_user_path(@user)
    assert_select 'a', text: 'Delete comment.', count: 1
  end
end
