# frozen_string_literal: true

require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:bilbo)
    log_in_as(@user)
  end

  test 'following page' do
    get following_user_path(@user)
    assert_not @user.following.empty?
    assert_match @user.followers.count.to_s, response.body
    # TODO(Genevieve) Complete these tests!
    # @user.followers.each do |user|
    #   assert_select 'a[href=?]', user_path(user)
    # end
  end
end
