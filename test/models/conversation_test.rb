require 'test_helper'

class ConversationTest < ActiveSupport::TestCase
  def setup
    @conversation = Conversation.new(commenter_id: users(:bilbo).id, recipient_id: users(:samwise).id,
                                     comment: 'Lorem ipsum')
  end

  test 'should be valid' do
    assert @conversation.valid?
  end

  test 'should require a commenter_id' do
    @conversation.commenter_id = nil
    assert_not @conversation.valid?
  end

  test 'should require a recipient_id' do
    @conversation.recipient_id = nil
    assert_not @conversation.valid?
  end

  test 'comment should not be empty' do
    @conversation.comment = nil
    assert_not @conversation.valid?
  end

  test 'comment should be no more than 500 characters' do
    @conversation.comment = 'a' * 501
    assert_not @conversation.valid?
  end
end
