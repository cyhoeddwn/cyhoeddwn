# frozen_string_literal: true

require 'test_helper'

class ModerationTest < ActiveSupport::TestCase
  def setup
    @moderation = Moderation.new(moderator_id: users(:bilbo).id, review_id: reviews(:objection).id)
  end

  test 'should be valid' do
    assert @moderation.valid?
  end

  test 'should require a user id' do
    @moderation.moderator_id = nil
    assert_not @moderation.valid?
  end

  test 'should require a review id' do
    @moderation.review_id = nil
    assert_not @moderation.valid?
  end
end
