require "test_helper"

class ObservershipTest < ActiveSupport::TestCase
  def setup
    @observership = Observership.new(watcher_id: users(:bilbo).id, watched_id: users(:samwise).id)
  end

  test 'should be valid' do
    assert @observership.valid?
  end

  test 'should require a watcher_id' do
    @observership.watcher_id = nil
    assert_not @observership.valid?
  end

  test 'should require a watched_id' do
    @observership.watched_id = nil
    assert_not @observership.valid?
  end
end
