# frozen_string_literal: true

require 'test_helper'

class ReviewTest < ActiveSupport::TestCase
  def setup
    @post = posts(:what)
    @review = @post.reviews.build(comment: 'this is a test review', approved: true)
  end

  test 'should be valid' do
    assert @review.valid?
  end

  test 'post id should be present' do
    @review.post_id = nil
    assert_not @review.valid?
  end

  test 'approved should be present' do
    @review.approved = nil
    assert_not @review.valid?
  end

  test 'comment should be present' do
    @review.comment = ''
    assert_not @review.valid?
  end

  test 'comment should be 500 characters at most' do
    @review.comment = 'a' * 501
    assert_not @review.valid?
  end

  test 'order should be most recent first' do
    assert_equal reviews(:most_recent), Review.first
  end
end
