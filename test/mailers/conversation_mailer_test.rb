require "test_helper"

class ConversationMailerTest < ActionMailer::TestCase
  test "new_comment_own_talkpage" do
    user = users(:bilbo)
    conversation = user.received_comments.first
    mail = ConversationMailer.new_comment_own_talkpage(user)
    assert_equal "New comment from #{conversation.commenter.name} on your cyhoeddwn talk page", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "new_comment_watching" do
    user = users(:bilbo)
    conversation = user.received_comments.first
    mail = ConversationMailer.new_comment_watching(user)
    assert_equal "New comment on the talk page of #{user.name} on cyhoeddwn", mail.subject
    assert_equal ['pippin@example.me'], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
