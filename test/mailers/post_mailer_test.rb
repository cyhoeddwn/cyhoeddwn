require "test_helper"

class PostMailerTest < ActionMailer::TestCase
  test "new_post_watching" do
    post = posts(:apple)
    mail = PostMailer.new_post_watching(post)
    assert_equal "New post on cyhoeddwn from #{post.user.name}", mail.subject
    assert_equal ['pippin@example.me'], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
