# Preview all emails at http://localhost:3000/rails/mailers/conversation_mailer
class ConversationMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/conversation_mailer/new_comment_own_talkpage
  def new_comment_own_talkpage
    user = User.first
    ConversationMailer.new_comment_own_talkpage(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/conversation_mailer/new_comment_watching
  def new_comment_watching
    user = User.last
    ConversationMailer.new_comment_watching(user)
  end

end
