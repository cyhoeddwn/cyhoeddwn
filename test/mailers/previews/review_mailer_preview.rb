# Preview all emails at http://localhost:3000/rails/mailers/review_mailer
class ReviewMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/review_mailer/post_approved
  def post_approved
    post = Post.first
    ReviewMailer.post_approved(post)
  end

  # Preview this email at http://localhost:3000/rails/mailers/review_mailer/post_disapproved
  def post_disapproved
    ReviewMailer.post_disapproved
  end

end
