require "test_helper"

class ReviewMailerTest < ActionMailer::TestCase
  test "post_approved" do
    post = posts(:what)
    mail = ReviewMailer.post_approved(post)
    assert_equal "Your post has been approved!", mail.subject
    assert_equal [post.user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match post.user.name, mail.body.encoded
    assert_match post.content, mail.body.encoded
  end

  test "post_disapproved" do
  end

end
