# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: 'Jane Allsop',
             email: 'example@somedomain.com',
             password: 'foobar',
             password_confirmation: 'foobar',
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

14.times do |n|
  name = Faker::Name.name
  email = "example-#{n + 1}@somedomain.com"
  password = 'password'
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

# You should be able to define this with some kind of yml file, but I can't for the life of me work out how

def create_text_post(user, text)
  user.posts.create(content: text)
  user.posts.first
end

def create_image_post(user, text, image_name)
  image = File.open("app/assets/images/seed/#{image_name}")
  user.posts.create(content: text).image.attach(io: image, filename: image_name)
  user.posts.first
end

def create_review(review_author, post, comment, approved)
  review = post.reviews.build(comment: comment, approved: approved)
  review_author.add_review(review)
  review.save
  post.approval_status = post.check_approval_criteria
  post.save
  review
end

def create_conversation(commenter, recipient, comment)
  conversation = Conversation.new(commenter: commenter, recipient: recipient, comment: comment)
  conversation.save
  conversation
end

users = User.all

# Story setup

# cyhoeddwn administrator works at the library and volunteers her time to do admin stuff

users[0].bio = 'Volunteer cyhoeddwn admin, I\'m a librarian by trade. Northern soul for life. Prounouns are she/her.'
users[0].save

# 5th user disagrees with almost everything, is a capital C 'Conservative', contrarian, etc., also hates public transport
# as much as Dr Beeching and the Margaret Thatcher
users[4].bio = 'Conservative Councillor for West Exampleworth, I run constituent surgeries every Wednesday at 1:30pm
 in Central Library'
users[4].name = 'Cllr. Bob Hackett'
users[4].save

# Food Bank post (3 approvals, 1 disapprovals)
food_bank = create_image_post(users[0],
                              'Thanks to your generous support, we are now able to open Sampleton Food Bank!',
                              'sampleton_food_bank.png')

create_review(users[1], food_bank, 'Will be popping by later this week! Graphics look nice!', true)
create_review(users[2], food_bank,
              'Wish we didn\'t have to open this, but glad we can help our local residents, like the colour scheme!',
              true)
create_review(users[3], food_bank, 'This has my blessing!', true)
create_review(users[4], food_bank, 'What is this nonsense? Socialism, in Sampleton? No chance.', false)

# Selling a car (4 approvals, 0 disapprovals)
car = create_text_post(users[6], 'Selling a Ford Mundeo, 36k miles, good runner, \'51 plate and MOT\'d. £2200 o.n.o.')

create_review(users[7], car, 'Hope you can sell the rusty bunch of junk (just kidding!)', true)
create_review(users[12], car,
              'Sounds suspiciously like the car that got nicked off the village green a few weeks back, not a copper 
              though! Looks okay otherwise!', true)
create_review(users[13], car, 'It\'s ok', true)

# Anti-bus post (1 approval, 2 disapprovals)
bus = create_text_post(users[4],
                       'Say NO to extension of X52 bus to West Exampleworth! Vote Conservative to keep our ancient
                        roads preserved!')

create_review(users[7], bus,
              'You\'ve got my vote! Always hated buses since that bus driver crashed into my Rolls, absolute 
              nuisance!', true)
create_review(users[2], bus,
              'Not again, the DEMOCRATICALLY SELECTED rules have put a stop to your incessant party political 
              broadcasts, it has to end!', false)
create_review(users[3], bus,
              'Seconded, you must read the rules, what is your problem with affordable public transport for your 
              constituents anyway?.', false)

# Train club post (3 approvals, 2 disapprovals)
anoraks = create_image_post(users[8],
                            'Thanks to some seed funding from Sampleton Heritage Railway, we\'ll be opening Thursday 
                            22/03!', 'sampleton_anoraks.png')

create_review(users[4], anoraks, 'What a waste of community centre time', false)
create_review(users[5], anoraks, 'Looks good!', true)
create_review(users[6], anoraks, 'Hope you all have fun, can\'t make this meeting, but I\'ll be at the next one', true)
create_review(users[9], anoraks, 'lmao, imagine actually liking trains', false)
create_review(users[3], anoraks,
              'Nice graphic design! Have fun! Also, just a reminder to other reviewers, you\'re here to say if the 
              content is suitable for display, NOT to bash others for their views, hobbies, or opinions.
              We\'re a broad church!', true)

# Trip to Llandudno post (2 approvals, 0 disapprovals)
holiday = create_image_post(users[10], 'We\'ve organised a trip, please RSVP!', 'ladies_circle.png')

create_review(users[4], holiday,
              'Ah yes, it\'ll be good to be rid of the wife for a few days! (JUST JOKING to all you sensitive crybabies,
               don\'t cancel me!)', true)
create_review(users[11], holiday, 'I\'ll let Linda know about this, she\'s been wanting to go to Llandudno for ages!',
              true)

# Leaving comment on nuisance user's page

create_conversation(users[2], users[0], "Hi Jane! Thanks for letting me know about this system!")

create_conversation(users[0], users[4], "Hi Bob! Thanks for taking an interest in cyhoeddwn, great to see our
  local politicians getting involved. Could I politely ask that you tone down the political nature of your comments a 
  little, as this contravenes our no-politics rule?")

create_conversation(users[4], users[4], "Jane, with all due respect, absolutely not and get stuffed while 
  you're at it you cow.")

create_conversation(users[0], users[4], "This is a final warning for #{users[4].name}, further contravention of site
  rules will result in your account being suspended. This is due to: breaking rule 4.2 (ad-hominem) - 'get stuffed
  while you're at it you cow'. Remediation: remove offensive message from talkpage.")


create_conversation(users[6], User.last, 'lmao nice comment')
# Create following relationships
users = User.all
user = users.first
user.follow(users[1..])

# Create watching relationships
user.watch(users[1..])
