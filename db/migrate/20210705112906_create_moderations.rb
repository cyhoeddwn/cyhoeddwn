# frozen_string_literal: true

class CreateModerations < ActiveRecord::Migration[6.1]
  def change
    create_table :moderations do |t|
      t.integer :moderator_id
      t.integer :review_id

      t.timestamps
    end
    add_index :moderations, :moderator_id
    add_index :moderations, :review_id
    add_index :moderations, %i[moderator_id review_id], unique: true
  end
end
