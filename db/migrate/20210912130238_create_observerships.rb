class CreateObserverships < ActiveRecord::Migration[6.1]
  def change
    create_table :observerships do |t|
      t.integer :watcher_id
      t.integer :watched_id

      t.timestamps
    end
    add_index :observerships, :watcher_id
    add_index :observerships, :watched_id
    add_index :observerships, %i[watcher_id watched_id], unique: true
  end
end
