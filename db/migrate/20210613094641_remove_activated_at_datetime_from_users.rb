# frozen_string_literal: true

class RemoveActivatedAtDatetimeFromUsers < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :activated_at, :string
    remove_column :users, :datetime, :string
  end
end
