# frozen_string_literal: true

class CreateReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :reviews do |t|
      t.text :comment
      t.boolean :approved
      t.references :post, null: false, foreign_key: true

      t.timestamps
    end
    add_index :reviews, %i[post_id created_at]
  end
end
