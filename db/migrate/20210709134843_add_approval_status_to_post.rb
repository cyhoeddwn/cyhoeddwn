# frozen_string_literal: true

class AddApprovalStatusToPost < ActiveRecord::Migration[6.1]
  def change
    add_column :posts, :approval_status, :boolean, default: false
  end
end
