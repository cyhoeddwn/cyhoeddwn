class CreateConversations < ActiveRecord::Migration[6.1]
  def change
    create_table :conversations do |t|
      t.integer :commenter_id
      t.integer :recipient_id
      t.text :comment

      t.timestamps
    end
    add_index :conversations, :commenter_id
    add_index :conversations, :recipient_id
    add_index :conversations, %i[commenter_id recipient_id created_at],
              name: 'index_convn_on_commenter_id_and_recipient_id_and_created_at'
  end
end
