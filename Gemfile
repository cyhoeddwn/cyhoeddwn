# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 3.0.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 6.1.3', '>= 6.1.3.2'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'listen', '~> 3.3'
  gem 'rack-mini-profiler', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'guard', '~> 2.17'
  gem 'guard-minitest', '~> 2.4'
  gem 'webdrivers'

  gem 'minitest', '~> 5.14'
  gem 'minitest-reporters', '~> 1.4'
end

group :production do
  gem 'aws-sdk-s3', require: false
  gem 'resque'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'rexml', '~> 3.2'

gem 'travis', '~> 1.8'

gem 'bcrypt', '~> 3.1'

gem 'rails-controller-testing', '~> 1.0'

gem 'sdoc', '~> 2.2'

gem 'yard', '~> 0.9.26'

gem 'faker', '~> 2.18'

gem 'bootstrap-will_paginate', '~> 1.0'
gem 'will_paginate', '~> 3.3'

gem 'capistrano', '~> 3.16'
gem 'capistrano-passenger', '~> 0.2.1'
gem 'capistrano-rails', '~> 1.6'
gem 'capistrano-rbenv', '~> 2.2'

gem 'ed25519', '~> 1.2'

gem 'bcrypt_pbkdf', '~> 1.1'

gem 'passenger', '~> 6.0', group: :production

gem 'active_storage_validations', '~> 0.9.5'

gem 'image_processing', '~> 1.12'
gem 'mini_magick', '~> 4.11'

gem 'figaro', '~> 1.2'
