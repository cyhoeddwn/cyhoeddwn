# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include SessionsHelper

  private

  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = 'You\'ll need to be logged in to perform that action'
      redirect_to login_url
    end
  end

  def check_approval_criteria(post)
    # Post not approved if there are no reviews
    return false if post.reviews.empty?

    # Post not approved if there are less than three positive reviews
    positive_reviews = post.reviews.where(approved: true).count
    return false if positive_reviews < 3

    # Post not approved if number of negative views >= 50%
    negative_reviews = post.reviews.where(approved: false).count
    return false if negative_reviews >= positive_reviews

    # Return true if none of the above are met
    true
  end
end
