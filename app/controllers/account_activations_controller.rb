# frozen_string_literal: true

class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = 'Your account has been activated!'
      redirect_to root_url
    else
      flash[:danger] = 'Oops! That appears to be an invalid activation link!'
      redirect_to root_url
    end
  end
end
