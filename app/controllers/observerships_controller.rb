class ObservershipsController < ApplicationController
  before_action :logged_in_user

  def create
    @user = User.find(params[:watched_id])
    current_user.watch(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Observership.find(params[:id]).watched
    current_user.unwatch(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
