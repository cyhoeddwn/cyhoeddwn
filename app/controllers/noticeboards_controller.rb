class NoticeboardsController < ApplicationController
  before_action :logged_in_user
  layout 'no_hf'

  def show
    @posts = current_user.feed.where(approval_status: true)
    @post = @posts.sample
  end
end
