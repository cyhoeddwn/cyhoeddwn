# frozen_string_literal: true

class RelationshipsController < ApplicationController
  before_action :logged_in_user

  # TODO(Genevieve): Create equivalent js for these actions 14.39/14.40/14.41

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
