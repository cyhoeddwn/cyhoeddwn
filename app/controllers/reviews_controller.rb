# frozen_string_literal: true

class ReviewsController < ApplicationController
  before_action :logged_in_user, only: %i[create new destroy]
  before_action :not_self_user, only: %i[new create]
  before_action :disallow_duplicate_review, only: %i[new create]

  def new
    @post = Post.find(params[:id])
    @review = Review.new
  end

  def create
    @review = Review.new
    @post = Post.find(params[:post_id])
    @review = @post.reviews.build(review_params)
    @user = current_user
    @user.add_review(@review)
    @review.save
    update_approval_status(@post)
    flash[:success] = 'You have left a review!'
    redirect_to @post
  rescue StandardError
    render 'reviews/new'
  end

  def destroy
    @review = Review.find(params[:id])
    @post = @review.post
    @review.destroy
    update_approval_status(@post)
    flash[:success] = 'Review deleted!'
    redirect_to request.referrer || root_url
  end

  private

  def review_params
    params.require(:review).permit(:comment, :approved, :author)
  end

  def not_self_user
    @post = Post.find_by(id: params[:post_id] || params[:id])
    @post_author = @post.user
    if @post_author == current_user
      flash[:danger] = 'You can\'t add a review for your own post!'
      redirect_to root_url
    end
  end

  def disallow_duplicate_review
    @post = Post.find_by(id: params[:post_id] || params[:id])
    @reviews = @post.reviews
    return if @reviews.empty?

    @reviews.each do |review|
      next unless review.author == current_user

      flash[:danger] = 'You\'ve already left a review for this post!'
      redirect_to @post
    end
  end

  def update_approval_status(post)
    old_approval = post.approval_status
    post.approval_status = check_approval_criteria(@post)
    post.save
    ReviewMailer.post_approved(post).deliver_now if !old_approval && post.approval_status
  end
end
