# frozen_string_literal: true

class ConversationsController < ApplicationController
  before_action :logged_in_user, only: %i[create destroy edit]
  before_action :no_delete_others, only: :destroy

  def create
    @recipient = User.find_by(id: params[:recipient_id])
    @conversation = Conversation.new(comment: params[:conversation][:comment], recipient: @recipient,
                                     commenter: current_user)
    if @conversation.save
      flash[:success] = 'You have left a comment on the user\'s talk page!'
      ConversationMailer.new_comment_own_talkpage(@recipient).deliver_now
      ConversationMailer.new_comment_watching(@recipient).deliver_now
    end
    redirect_to received_comments_user_path(params[:recipient_id])
  end

  def destroy
    @conversation = Conversation.find_by(id: params[:id])
    @conversation.destroy
    flash[:success] = 'Comment deleted!'
    redirect_to request.referrer || root_url
  end

  private

  def no_delete_others
    @conversation = Conversation.find_by(id: params[:id])
    @commenter = @conversation.commenter
    if @commenter != current_user
      flash[:danger] = 'You can only delete your own comments!'
      redirect_to root_url
    end
  end

  def conversation_params
    params.require(:conversation).permit(:recipient_id, :commenter_id, :comment)
  end
end
