# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :logged_in_user, only: %i[create destroy update edit show]
  before_action :correct_user, only: %i[destroy update edit]
  after_action :update_approval_status, only: :update
  def create
    @post = current_user.posts.build(post_params)
    @post.image.attach(params[:post][:image])
    if @post.save
      PostMailer.new_post_watching(@post).deliver_now
      flash[:success] = 'Post created!'
      redirect_to root_url
    else
      @feed_items = current_user.feed.paginate(page: params[:page])
      render 'welcome/index'
    end
  end

  def destroy
    @post.destroy
    flash[:success] = 'Post deleted!'
    redirect_to request.referrer || root_url
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @user = User.find(@post.user_id)
    if @post.update(post_params)
      @post.reviews.map(&:destroy)
      flash[:success] = 'Post updated'
      redirect_to @user
    else
      render 'edit'
    end
  end

  def show
    @post = Post.find(params[:id])
    @reviews = @post.reviews.paginate(page: params[:page])
  end

  private

  def post_params
    params.require(:post).permit(:content, :image)
  end

  def correct_user
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to root_url if @post.nil?
  end

  def update_approval_status
    @post.approval_status = check_approval_criteria(@post)
    @post.save
  end
end
