function docReady(fn) {
  if (document.readyState === "complete" || document.readyState === "interactive") {
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

docReady(function() {
  setInterval(refreshPartial, 10000)
});

function refreshPartial() {
  window.location.reload();
}