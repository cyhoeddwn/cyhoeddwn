class PostMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.post_mailer.new_post_watching.subject
  #
  def new_post_watching(post)
    @watchers = post.user.watchers
    @watchers.each do |watcher|
      new_post_watching_internal(post, watcher)
    end
  end

  def new_post_watching_internal(post, watcher)
    @post = post
    @watcher = watcher
    email = @watcher.email
    mail(to: email, subject: "New post on cyhoeddwn from #{@post.user.name}")
  end
end
