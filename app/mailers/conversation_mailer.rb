class ConversationMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.conversation_mailer.new_comment_own_talkpage.subject
  #
  def new_comment_own_talkpage(user)
    @user = user
    @conversation = user.received_comments.first
    mail to: user.email, subject: "New comment from #{@conversation.commenter.name} on your cyhoeddwn talk page"
  end

  def new_comment_watching(user)
    @user = user
    @watchers = user.watchers
    @watchers.each do |watcher|
      new_comment_watching_internal(user, watcher)
    end
  end

  def new_comment_watching_internal(user, watcher)
    @user = user
    @watcher = watcher
    @conversation = user.received_comments.first
    email = @watcher.email
    mail(to: email, subject: "New comment on the talk page of #{@user.name} on cyhoeddwn")
  end
end
