class ReviewMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.review_mailer.post_approved.subject
  #
  def post_approved(post)
    @post = post
    @user = @post.user
    mail to: @user.email, subject: 'Your post has been approved!'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.review_mailer.post_disapproved.subject
  #
  def post_disapproved(post)
    @post = post
    @user = @post.user
    mail to: @user.email, subject: 'Your post has been rejected.'
  end
end
