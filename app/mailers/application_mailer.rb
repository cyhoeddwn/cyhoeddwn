# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: ENV['CYHOEDDWN_EMAIL_FROM']
  helper UsersHelper
  layout 'mailer'
end
