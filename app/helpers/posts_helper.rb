# frozen_string_literal: true

module PostsHelper
  def positive_reviews(post)
    post.reviews.where(approved: true).count
  end

  def negative_reviews(post)
    post.reviews.where(approved: false).count
  end

  def review_count(post)
    if post.reviews.empty?
      nil
    else
      positive = positive_reviews(post)
      negative = negative_reviews(post)
      "(+#{positive} -#{negative})"
    end
  end
end
