# frozen_string_literal: true

module ApplicationHelper
  def full_title(page_title = '')
    base_title = 'cyhoeddwn'
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def display_post_number(post_number)
    "Post #{post_number}"
  end
end
