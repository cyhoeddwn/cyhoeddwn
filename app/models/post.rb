# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :user
  has_many :reviews, dependent: :destroy
  has_one_attached :image
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validates :image, content_type: { in: %w[image/jpeg image/gif image/png],
                                    message: 'must be a valid image format' },
                    size: { less_than: 5.megabytes,
                            message: 'should be less than 5MB' },
                    aspect_ratio: :landscape,
                    dimension: { width: { min: 1000 },
                                 height: { min: 500 },
                                 message: 'image too small (dimensions)' }

  def display_image
    image.variant(resize_to_limit: [500, 500])
  end

  def make_review(user, comment: 'I approve your content!', approved: true)
    review = reviews.build(comment: comment, approved: approved)
    user.add_review(review)
    save
  end

  def check_approval_criteria
    # Post not approved if there are no reviews
    return false if reviews.empty?

    # Post not approved if there are less than three positive reviews
    positive_reviews = reviews.where(approved: true).count
    return false if positive_reviews < 3

    # Post not approved if number of negative views >= 50%
    negative_reviews = reviews.where(approved: false).count
    return false if negative_reviews >= positive_reviews

    # Return true if none of the above are met
    true
  end
end
