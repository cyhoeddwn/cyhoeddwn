# frozen_string_literal: true

class Moderation < ApplicationRecord
  belongs_to :moderator, class_name: 'User'
  belongs_to :review
  validates :moderator_id, presence: true
  validates :review_id, presence: true
end
