# frozen_string_literal: true

class Review < ApplicationRecord
  belongs_to :post
  default_scope -> { order(created_at: :desc) }
  validates :post_id, presence: true
  validates :approved, inclusion: { in: [true, false] }
  validates :comment, presence: true, length: { maximum: 500 }
  has_one :reverse_moderation, class_name: 'Moderation', foreign_key: 'review_id', dependent: :destroy
  has_one :author, through: :reverse_moderation, source: :moderator
end
