class Conversation < ApplicationRecord
  belongs_to :commenter, class_name: 'User'
  belongs_to :recipient, class_name: 'User'
  validates :commenter_id, presence: true
  validates :recipient_id, presence: true
  validates :comment, presence: true, length: { maximum: 500 }
  default_scope -> { order(created_at: :desc) }
end
